# https://woolworthsdigital.atlassian.net/wiki/spaces/DGDMS/pages/1514144957/Feature+Store+Python+Client+Documentation#FeatureStorePythonClientDocumentation-InstallationandOn-boarding

# use GCP service account
# !export GOOGLE_APPLICATION_CREDENTIALS=<path/to/service-account.json>


# some other way to authenticat? !gcloud auth activate-service-account argo-service-account@gcp-wow-rwds-ai-dschapter-dev.iam.gserviceaccount.com --key-file=argo-dschapter-dev-c9b15ee5d63f.json


conda create -n feature-storm ipykernel

source activate feature-storm
pip install keyrings.google-artifactregistry-auth
gcloud auth login
pip install --extra-index-url https://us-pypi.pkg.dev/gcp-wow-rwds-ai-mleops-prod/feature-store-pypi/simple/ feature-store-python-client

# alternate installation method
# 1. Download and install wow-api-python-client package
# gsutil cp gs://wx-lty-de-prod-releases/wow-api-python-client/wow-api-python-client-latest.tar.gz .
# pip install wow-api-python-client-latest.tar.gz
# 2. Download and install feature-store-python-client package
# gsutil cp gs://wx-lty-de-prod-releases/feature-store-python-client/feature-store-python-client-latest.tar.gz .
# pip install feature-store-python-client-latest.tar.gz