import datetime
from feature_store import FeatureStore
from feature_store.model import Feature

fs = FeatureStore(env="test")

# create feature set
response = fs.management.define_feature_set(
    feature_group_name='ds_tooling_chapter',
    feature_set_name='demo_feature_set_AL',
    key_columns=['crn', 'subcat'],
 
    # optionally define description/meta
    description="feature set level description",
    meta={"key1": "feature set level meta"},
 
    # optionally define owners/viewers
#     , 'argo-service-account@gcp-wow-rwds-ai-dschapter-dev.iam.gserviceaccount.com'
    owners = ['alau3@woolworths.com.au'],
    # # if no viewers are assigned, everyone can view the feature, otherwise only the specified user can view the feature.

    features=[
        Feature('dm__f1', 'f1 description', {'key1': 'value1'}),
        Feature('dm__f2', 'f2 description', {'key2': 'value2'}),
    ],
 )

# # above works when run by itself
print("fs.management.list_feature_group()", fs.management.list_feature_group())
print("fs.management.list_feature_set('ds_tooling_chapter')", fs.management.list_feature_set('ds_tooling_chapter'))
print("fs.management.get_feature_set('ds_tooling_chapter', 'demo_feature_set_AL')",  fs.management.get_feature_set('ds_tooling_chapter', 'demo_feature_set_AL'))

# Feature Ingestion

# Set reference timestamp
ref_ts = datetime.datetime.now(datetime.timezone.utc)

# Load features from SQL script.
bq_sql = """
    SELECT '999' as crn, 'cat5' as subcat, 101 as dm__f1, 202 as dm__f2;
 
    INGEST_FEATURE_FROM_LAST_STATEMENT(
        group='ds_tooling_chapter',
        name='demo_feature_set_AL',
        owners='user:alau3@woolworths.com.au',
        keys='crn,subcat',
        meta='{ brd: "super" }',
        features='{dm__f1: { desc: "f1 desc"}, dm__f2: { desc: "f2 desc"} }'
    );
"""

fs.batch.ingest_features_from_sql(sql=bq_sql, ref_ts=ref_ts)